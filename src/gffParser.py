#!/usr/bin/env python3

import DataModel


def get_gff_features(input_file):
    gff_items = list()
    with open(input_file) as file_handle:
        for line in file_handle:
            if not line.startswith('#') and line != '':
                line = line.strip()
                fields = line.split('\t')
                if len(fields) == 9:
                    gff_obj = DataModel.GffFeature(fields)
                    gff_items.append(gff_obj)
                else:
                    print("not enough attributes")
    return gff_items
