#!/usr/bin/env python3


from routeFile import route_file
import os


from flask import Flask, request, render_template, flash, redirect, url_for, make_response
from werkzeug.utils import secure_filename

app = Flask(__name__)

ALLOWED_EXTENSIONS = set(['bed', 'fastq', 'fasta', 'gff', 'sam', 'vcf', 'gff3'])
UPLOAD_FOLDER = '/Users/jasperbosman/Documents/Developments/thema03_flask/src/UPLOAD_FOLDER/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/')
def index():
    return render_template('baseTemplate.html')


@app.route('/Form')
def load_form():
    return render_template('formTemplate.html')


@app.route('/make_graph', methods=['GET', 'POST'])
def make_graph():
    if request.method == 'POST':
        file = request.files['file']
        if allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            in_file = (os.path.join(app.config['UPLOAD_FOLDER'], filename))
            file_features = route_file(in_file)
            print(file_features[0].seqname)
            labels = list()
            values = list()
            for atr in file_features[0].__dict__.keys():
                if isinstance(getattr(file_features[0], atr), int):
                    labels.append(atr)
                    values.append(getattr(file_features[0], atr))

            return render_template('plotTemplate.html', values=values, labels=labels)


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


if __name__ == '__main__':
    app.run(debug=True)
