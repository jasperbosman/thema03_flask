#!/usr/bin/env python3


class GffFeature:
    def __init__(self, fields):
        self.seqname = fields[0]
        self.source = fields[1]
        self.feature = fields[2]
        self.start = int(fields[3])
        self.end = int(fields[4])
        self.score = fields[5]
        self.strand = fields[6]
        self.frame = fields[7]

        self.attributes = dict()
        self.parse_attributes(fields[8])

    def parse_attributes(self, fields):
        self.attributes = dict()
        for field in fields.split(';'):
            items = field.split('=')
            self.attributes[items[0]] = items[1]


class BedFeature:
    def __init__(self, fields):
        self.chrom = fields[0]
        self.chrom_start = fields[1]
        self.chrom_end = fields[2]
        self.name = fields[3]
        self.score = fields[4]
        self.strand = fields[5]
        self.thick_start = fields[6]
        self.thick_end = fields[7]
        self.item_rbg = fields[8]
        self.block_count = fields[9]
        self.block_sizes = fields[10]
        self.block_starts = fields[11]




def get_fastq_features(input_file):
    pass


def get_fasta_features(input_file):
    pass


def get_sam_features(input_file):
    pass


def get_vcf_features(input_file):
    pass


def get_gff3_features(input_file):
    pass
