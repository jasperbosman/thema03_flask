#!/usr/bin/env python3

import gffParser


def route_file(file):
    print(file)
    if file[-4:] == 'gff3':
        return(gffParser.get_gff_features(file))
    else:
        print("No such file type allowed")