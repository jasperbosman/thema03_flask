#!/usr/bin/env python3



def get_bed_features(input_file):
    bed_items = list()
    with open(input_file) as file_handle:
        for line in file_handle:
            line = line.strip()
            fields = line.split('\t')
            bed_obj = bed_feature(fields)
            bed_items.append(bed_obj)